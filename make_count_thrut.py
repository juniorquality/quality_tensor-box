import argparse
import json
import os
import time

import cv2


def save_file(name_file, obj):
    with open(name_file, 'w') as f:
        json.dump(obj, f, indent=4)


pwd_path = os.path.dirname(os.path.realpath(__file__))


parser = argparse.ArgumentParser()
parser.add_argument("file")
args = parser.parse_args()

file_name = str(args.file)
full_file_path = os.path.join(pwd_path, file_name)

dir_json_file = os.path.dirname(full_file_path)

dataset = None

with open(full_file_path, 'r') as f:
    dataset = json.load(f)

new_dataset = []
for d in dataset:
    new_obj = {}

    img = cv2.imread(os.path.join(dir_json_file, d['image_path']))
    cv2.imshow('image', img)

    new_obj['image_path'] = str(d['image_path'])

    result = ""
    finish = False
    while True:
        key = cv2.waitKey(0) & 0xFF
        if key == ord('q'):
            finish = True
            break
        if key == ord('s'):
            if len(result) == 0:
                continue
            break
        if key == ord('r'):
            result = result[-1]
            if len(result) == 1:
                result = ""
        if key >= ord('0') and key <= ord('9'):
            result += str(chr(key))
        print(result)
    save_file(os.path.join(
        dir_json_file, 'people_counting_{}.json'.format(
            'temp')), new_dataset)
    if finish:
        break

    new_obj['people_num'] = int(result)
    print(new_obj)
    new_dataset.append(new_obj)


save_file(os.path.join(
    dir_json_file, 'people_counting_{}.json'.format(
        int(time.time() * 1000))), new_dataset)
