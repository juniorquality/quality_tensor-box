import argparse
import json
import os
import time
import subprocess
import cv2
import tensorflow as tf
from scipy.misc import imresize

from train import build_forward
from utils.train_utils import add_rectangles
import multiprocessing as mp


def ReadRun(q):
    # cap = cv2.VideoCapture('rtsp://admin:admin@192.168.1.33:554/ch04.264')
    cap = cv2.VideoCapture('rtsp://admin:admin@192.168.1.33:554/ch07.264')

    # cap = cv2.VideoCapture('rtsp://200.153.119.37:554/ch02.264')
    while(cap.isOpened()):
        ret, img = cap.read()
        if not q.full():
            q.put(img, block=False)
        # time.sleep(0.05)
    return


def ViewRun(q):
    while True:
        try:
            if not q.empty():
                img = q.get(block=False)
                cv2.imshow('View', img)
                key = cv2.waitKey(1) & 0xFF
                if key == ord('q'):
                    break
                # print('Time ViewRun: {}'.format(time.time() - t))

            else:
                time.sleep(0.05)
        except Exception as e:
            print(e)


def get_results(args, H, r, v):

    tf.reset_default_graph()
    x_in = tf.placeholder(tf.float32, name='x_in', shape=[
                          H['image_height'], H['image_width'], 3])
    if H['use_rezoom']:
        pred_boxes, pred_logits, pred_confidences, pred_confs_deltas, pred_boxes_deltas = build_forward(
            H, tf.expand_dims(x_in, 0), 'test', reuse=None)
        grid_area = H['grid_height'] * H['grid_width']
        pred_confidences = tf.reshape(
            tf.nn.softmax(tf.reshape(
                pred_confs_deltas, [grid_area * H['rnn_len'], 2])), [grid_area, H['rnn_len'], 2])
        if H['reregress']:
            pred_boxes = pred_boxes + pred_boxes_deltas
    else:
        pred_boxes, pred_logits, pred_confidences = build_forward(
            H, tf.expand_dims(x_in, 0), 'test', reuse=None)
    saver = tf.train.Saver()
    img_path = H["save_dir"] + '/apply_results/'
    subprocess.call(["mkdir", "-p", img_path])
    number_idx = 0

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True

    with tf.Session(config=config) as sess:
        sess.run(tf.global_variables_initializer())
        saver.restore(sess, args.weights)
        while True:
            if not r.empty():
                orig_img = r.get(block=False)
                img = imresize(
                    orig_img, (H["image_height"], H["image_width"]), interp='cubic')
                feed = {x_in: img}
                (np_pred_boxes, np_pred_confidences) = sess.run(
                    [pred_boxes, pred_confidences], feed_dict=feed)
                new_img, rects = add_rectangles(
                    H, [img], np_pred_confidences, np_pred_boxes,
                    use_stitching=True, rnn_len=H['rnn_len'],
                    min_conf=args.min_conf, tau=args.tau,
                    show_suppressed=args.show_suppressed)
                rectangles = []
                img_name = "img_{}".format(int(time.time() * 1000))
                for rect_ in rects:
                    rectangles.append(
                        {"x1": rect_.x1,
                         "x2": rect_.x2,
                         "y1": rect_.y1,
                         "y2": rect_.y2}
                    )
                if len(rectangles) > 0 and number_idx % 20 == 0:
                    json_obj = [{"rects": rectangles,
                                 "image_path": img_path + img_name + '.jpg'}]
                    with open(img_path + img_name + '.json', 'w') as fp:
                        json.dump(json_obj, fp, indent=4)
                        cv2.imwrite(img_path + img_name + '.jpg', img)
                if not v.full():
                    v.put(imresize(
                        new_img, (orig_img.shape[0], orig_img.shape[1]), interp='cubic'), block=False)
                else:
                    time.sleep(0.05)
                number_idx += 1
            else:
                time.sleep(0.05)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', required=True)
    parser.add_argument('--expname', default='')
    parser.add_argument('--gpu', default=0)
    parser.add_argument('--logdir', default='output')
    parser.add_argument('--iou_threshold', default=0.5, type=float)
    parser.add_argument('--tau', default=0.25, type=float)
    parser.add_argument('--min_conf', default=0.2, type=float)
    parser.add_argument('--show_suppressed', default=True, type=bool)
    args = parser.parse_args()
    os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu)
    hypes_file = '%s/hypes.json' % os.path.dirname(args.weights)
    with open(hypes_file, 'r') as f:
        H = json.load(f)
    BUF_SIZE = 2
    r = mp.Queue(BUF_SIZE)
    v = mp.Queue(BUF_SIZE)

    read = mp.Process(target=ReadRun, args=(r,))
    read.daemon = True
    read.start()

    view = mp.Process(target=ViewRun, args=(v,))
    view.daemon = True
    view.start()

    get_results(args, H, r, v)


if __name__ == '__main__':
    main()
