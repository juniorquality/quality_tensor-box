#!/usr/bin/env python
import argparse
import json
import os
import time
import base64
from StringIO import StringIO

import cv2
import numpy as np
import Image
import tensorflow as tf
from flask import Flask, jsonify, request
from scipy.misc import imresize

from train import build_forward
from utils.train_utils import add_rectangles

app = Flask(__name__)

sess = None
args = None
H = None
x_in = None
pred_boxes = None
pred_confidences = None


def getImage(request):
    v = request.files['image']
    # Check if the file is one of the allowed types/extensions
    if v:
        sbuf = StringIO()
        sbuf.write(v.stream.read())
        pimg = Image.open(sbuf)
        pimg = np.array(pimg)
        if len(pimg.shape) == 0:
            pimg = cv2.cvtColor(pimg, cv2.COLOR_GRAY2RGB)
        return pimg


def get_sess():
    global sess, args, H, x_in, pred_boxes, pred_confidences
    tf.reset_default_graph()
    x_in = tf.placeholder(tf.float32, name='x_in', shape=[
                          H['image_height'], H['image_width'], 3])
    if H['use_rezoom']:
        pred_boxes, pred_logits, pred_confidences, pred_confs_deltas, pred_boxes_deltas = build_forward(
            H, tf.expand_dims(x_in, 0), 'test', reuse=None)
        grid_area = H['grid_height'] * H['grid_width']
        pred_confidences = tf.reshape(
            tf.nn.softmax(tf.reshape(
                pred_confs_deltas, [grid_area * H['rnn_len'], 2])), [grid_area, H['rnn_len'], 2])
        if H['reregress']:
            pred_boxes = pred_boxes + pred_boxes_deltas
    else:
        pred_boxes, pred_logits, pred_confidences = build_forward(
            H, tf.expand_dims(x_in, 0), 'test', reuse=None)
    saver = tf.train.Saver()

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True

    sess = tf.Session(config=config)
    sess.run(tf.global_variables_initializer())
    saver.restore(sess, args.weights)
    return


@app.route('/counting_people', methods=['POST'])
def count():
    global sess, args, H, x_in, pred_boxes, pred_confidences
    orig_img = getImage(request)

    img = imresize(
        orig_img, (H["image_height"], H["image_width"]), interp='cubic')
    feed = {x_in: img}
    (np_pred_boxes, np_pred_confidences) = sess.run(
        [pred_boxes, pred_confidences], feed_dict=feed)
    new_img, rects = add_rectangles(
        H, [img], np_pred_confidences, np_pred_boxes,
        use_stitching=True, rnn_len=H['rnn_len'],
        min_conf=args.min_conf, tau=args.tau,
        show_suppressed=args.show_suppressed)
    ret = {}
    rectangles = []
    for rect_ in rects:
        rectangles.append(
            {"x1": rect_.x1,
                "x2": rect_.x2,
                "y1": rect_.y1,
                "y2": rect_.y2}
        )

    ret['people_num'] = len(rectangles)
    ret['people_positions'] = rectangles
    res_img = imresize(
        new_img, (orig_img.shape[0], orig_img.shape[1]), interp='cubic')
    cnt = cv2.imencode('.jpg', res_img)[1]
    imgS = base64.encodestring(cnt)

    ret['image_annoted'] = imgS
    return jsonify(ret)


def main():
    global sess, H, args
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', required=True)
    parser.add_argument('--expname', default='')
    parser.add_argument('--gpu', default=0)
    parser.add_argument('--logdir', default='output')
    parser.add_argument('--iou_threshold', default=0.5, type=float)
    parser.add_argument('--tau', default=0.25, type=float)
    parser.add_argument('--min_conf', default=0.2, type=float)
    parser.add_argument('--show_suppressed', default=True, type=bool)
    args = parser.parse_args()
    os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu)
    hypes_file = '%s/hypes.json' % os.path.dirname(args.weights)
    with open(hypes_file, 'r') as f:
        H = json.load(f)

    get_sess()

    app.run(threaded=True, host='0.0.0.0', port=10000)


if __name__ == '__main__':
    main()
