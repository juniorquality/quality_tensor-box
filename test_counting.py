import argparse
import json
import os
import time
import subprocess
import cv2
import tensorflow as tf
from scipy.misc import imresize
import numpy as np
from train import build_forward
from utils.train_utils import add_rectangles
import multiprocessing as mp

radius = 10


def rescale_boxes_back(ori_img_dim, rects):
    factor_x = ori_img_dim[1] / 640.0
    factor_y = ori_img_dim[0] / 480.0
    ret_rects = []
    for r in rects:
        ret_rect = {"x1": r["x1"] * factor_x,
                    "x2": r["x2"] * factor_x,
                    "y1": r["y1"] * factor_y,
                    "y2": r["y2"] * factor_y}
        ret_rects.append(ret_rect)
    return ret_rects


def ReadRun(q, file_path):
    # cap = cv2.VideoCapture('rtsp://admin:admin@192.168.1.33:554/ch04.264')

    with open(file_path, 'r') as f:
        dataset = json.load(f)

    dir_json_file = os.path.dirname(file_path)

    for d in dataset:
        img = cv2.imread(os.path.join(dir_json_file, d["image_path"]))
        q.put((img, d["people_num"]), block=True)
        # time.sleep(0.05)
    return


def get_number_people(img, rects):
    back = np.zeros(img.shape[0:2], dtype=np.uint8)
    rects = rescale_boxes_back(back.shape, rects)
    for rect in rects:

        cv2.circle(back, (int((rect['x1'] + rect['x2']) / 2.),
                          int((rect['y1'] + rect['y2']) / 2.)),
                   radius, (255), -1)

        cv2.circle(img, (int((rect['x1'] + rect['x2']) / 2.),
                         int((rect['y1'] + rect['y2']) / 2.)),
                   radius, (0, 150, 255), -1)

    ret, markers = cv2.connectedComponents(back)
    return ret - 1


def ViewRun(q):
    results = []
    while True:
        try:

            img, gt, rectangles = q.get(block=True)
            print(rectangles)
            pred = get_number_people(img, rectangles)
            # pred = len(rectangles)
            results.append(pred == gt)
            print(gt, pred, pred == gt)
            cv2.imshow('View', img)

            key = cv2.waitKey(100) & 0xFF
            if key == ord('q'):
                break
            # print('Time ViewRun: {}'.format(time.time() - t))
            npres = np.array(results)
            print("Results {}/{} {:.2}".format(
                np.sum(
                    npres), npres.shape[0], np.sum(
                        npres) / float(npres.shape[0])))

        except Exception as e:
            print(e)


def get_results(args, H, r, v):

    tf.reset_default_graph()
    x_in = tf.placeholder(tf.float32, name='x_in', shape=[
                          H['image_height'], H['image_width'], 3])
    if H['use_rezoom']:
        pred_boxes, pred_logits, pred_confidences, pred_confs_deltas, pred_boxes_deltas = build_forward(
            H, tf.expand_dims(x_in, 0), 'test', reuse=None)
        grid_area = H['grid_height'] * H['grid_width']
        pred_confidences = tf.reshape(
            tf.nn.softmax(tf.reshape(
                pred_confs_deltas, [grid_area * H['rnn_len'], 2])), [grid_area, H['rnn_len'], 2])
        if H['reregress']:
            pred_boxes = pred_boxes + pred_boxes_deltas
    else:
        pred_boxes, pred_logits, pred_confidences = build_forward(
            H, tf.expand_dims(x_in, 0), 'test', reuse=None)
    saver = tf.train.Saver()
    img_path = H["save_dir"] + '/apply_results/'
    subprocess.call(["mkdir", "-p", img_path])
    number_idx = 0

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True

    with tf.Session(config=config) as sess:
        sess.run(tf.global_variables_initializer())
        saver.restore(sess, args.weights)
        while True:
            orig_img, Num = r.get(block=True)
            img = imresize(
                orig_img, (H["image_height"], H["image_width"]), interp='cubic')
            feed = {x_in: img}
            (np_pred_boxes, np_pred_confidences) = sess.run(
                [pred_boxes, pred_confidences], feed_dict=feed)
            new_img, rects = add_rectangles(
                H, [img], np_pred_confidences, np_pred_boxes,
                use_stitching=True, rnn_len=H['rnn_len'],
                min_conf=args.min_conf, tau=args.tau,
                show_suppressed=args.show_suppressed)
            rectangles = []
            img_name = "img_{}".format(int(time.time() * 1000))
            for rect_ in rects:
                rectangles.append(
                    {"x1": rect_.x1,
                        "x2": rect_.x2,
                        "y1": rect_.y1,
                        "y2": rect_.y2}
                )
            if len(rectangles) > 0 and number_idx % 20 == 0:
                json_obj = [{"rects": rectangles,
                             "image_path": img_path + img_name + '.jpg'}]
                with open(img_path + img_name + '.json', 'w') as fp:
                    json.dump(json_obj, fp, indent=4)
                    cv2.imwrite(img_path + img_name + '.jpg', img)

            v.put((imresize(
                new_img, (
                    orig_img.shape[0],
                    orig_img.shape[1]), interp='cubic'), Num,
                   rectangles), block=True)

            number_idx += 1


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', required=True)
    parser.add_argument('--expname', default='')
    parser.add_argument('--gpu', default=0)
    parser.add_argument('--logdir', default='output')
    parser.add_argument(
        '--gt', default='data/img/people_counting_1497463527519.json')
    parser.add_argument('--iou_threshold', default=0.5, type=float)
    parser.add_argument('--tau', default=0.25, type=float)
    parser.add_argument('--min_conf', default=0.2, type=float)
    parser.add_argument('--show_suppressed', default=True, type=bool)
    args = parser.parse_args()
    os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu)
    hypes_file = '%s/hypes.json' % os.path.dirname(args.weights)
    with open(hypes_file, 'r') as f:
        H = json.load(f)
    BUF_SIZE = 20
    r = mp.Queue(BUF_SIZE)
    v = mp.Queue(BUF_SIZE)

    read = mp.Process(target=ReadRun, args=(r, args.gt))
    read.daemon = True
    read.start()

    view = mp.Process(target=ViewRun, args=(v,))
    view.daemon = True
    view.start()

    get_results(args, H, r, v)


if __name__ == '__main__':
    main()
