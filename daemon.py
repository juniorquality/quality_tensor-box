#!/usr/bin/env python
import subprocess
import os
import logging

logging.basicConfig(filename='daemon.log', level=logging.DEBUG)
dir_path = os.path.dirname(os.path.realpath(__file__))

service = 'ws_people_counting.py'
command_line = '{}/{} --weights {}/models/model1'.format(
    dir_path, service, dir_path)

p = subprocess.Popen(command_line.split(' '))
while True:
    p.wait()
    logging.debug('{} reexec.'.format(service))
    p = subprocess.Popen(command_line.split(' '))

